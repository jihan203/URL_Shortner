# URL Shortner

URL shortner provides short urls for all your long links

* Ruby version: 2.4.1

* Database: Mysql

* Tests: run rspec

* API Versioning is implemented through headers, specifiy the version number in the accept header value like this (application/vnd.url_shortner.2+json)

* Documentation: run rdoc
