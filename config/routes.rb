Rails.application.routes.draw do

  concern :api_base do
    get '/:short_url', to: 'links#show'
    resources :links, only: :create
  end

  scope module: :v1, constraints: ApiVersion.new('v1', true) do
    concerns :api_base
  end

end
