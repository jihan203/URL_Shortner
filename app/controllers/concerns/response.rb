# Generic response module to be shared across all controllers
module Response
  # Wraps the json response with `ok` status as default
  def json_response(object, status = :ok)
    render json: object, status: status
  end
end
