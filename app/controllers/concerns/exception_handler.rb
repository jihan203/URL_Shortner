module ExceptionHandler
  # to use the 'included' method
  extend ActiveSupport::Concern

  # Define custom error subclasses - rescue catches 'StandardErrors'
  class InvalidInput < StandardError; end

  included do
    rescue_from ActiveRecord::RecordNotFound do |e|
      json_response({ message: e.message }, :not_found)
    end

    # Define custom handlers
    rescue_from ActiveRecord::RecordInvalid, with: :four_twenty_two

  private

    # JSON response with message; Status code 422 - unprocessable entity
    def four_twenty_two(e)
      json_response({ message: e.message }, :unprocessable_entity)
    end

  end
end
