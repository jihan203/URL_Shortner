class V1::LinksController < ApplicationController

  # Saves a new link if the original/long url is new and returns the decorated link's object, the
  # base_url is passed to the decorator to represent the short url relative to the application's base url
  # instead of saving the full short urls in the database and having to change them in case the application's
  # domain changed
  def create
    link = Link.find_by_original_url link_params[:original_url]
    unless link
      link = Link.create! link_params.merge(short_url: Link.new_token)
    end
    json_response link.decorate(context: {base_url: request.base_url}), :created
  end

  # Takes the short_url as a parameter and redirects to the original url if exists or responding with
  # 404 response if not
  def show
    link = Link.find_by_short_url! params[:short_url]
    redirect_to link.original_url
  end

  private

  def link_params
    params.require(:link).permit(
      :original_url
    )
  end

end
