# == Schema Information
#
# Table name: links
#
#  id           :bigint(8)        not null, primary key
#  original_url :text(65535)
#  short_url    :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Link < ApplicationRecord

  ## Validations

  validates :original_url, :short_url, presence: true, uniqueness: true

  ## Methods

  # Returns a unique random token to be saved as link's short url, it consists of 2 parts:
  # <tt>random token</tt> concatenated with the <tt>link's id</tt> to make sure the short url is unique
  def self.new_token
    (Link.any? ? Link.last.id + 1 : 1).to_s.concat(SecureRandom.hex(6))
  end

end
