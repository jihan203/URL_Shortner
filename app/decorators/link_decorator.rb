class LinkDecorator < Draper::Decorator
  delegate :id, :original_url, :short_url, :created_at

  def short_url
    "#{context[:base_url]}/#{object.short_url}"
  end

end
