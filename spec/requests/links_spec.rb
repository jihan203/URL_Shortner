require 'rails_helper'

RSpec.describe 'Links API', type: :request do
  let(:link) { create(:link) }
  let(:headers) { valid_headers }
  let(:valid_attributes) do
    { link: { original_url: 'https://www.farmraiser.com/campaigns/brms-ottawa-trip-organic-seed-fundraiser' } }
  end

  # Create link test suite
  describe 'POST /links' do
    context 'valid request' do
      before { post '/links', params: valid_attributes.to_json, headers: headers }

      it 'returns 201 status code' do
        expect(response).to have_http_status(201)
      end

      it 'returns link\'s json object' do
        expect(json.keys).to match_array(["id", "short_url", "original_url", "created_at"])
      end

      it 'matches the input' do
        expect(json["original_url"]).to eq(valid_attributes[:link][:original_url])
      end
    end

    context 'database' do
      it 'saves the link object into the database' do
        expect { post '/links', params: valid_attributes.to_json, headers: headers }.to change{ Link.count }.by(1)
      end
    end

    context 'invalid request' do
      before { post '/links', params: {link: {original_url: ''}}.to_json, headers: headers }

      it 'returns 422 status code' do
        expect(response).to have_http_status(422)
      end
    end

  end

  # Redirect to the original link test suite
  describe 'GET /:short_url' do
    context 'valid request' do
      before { get "/#{link.short_url}", headers: headers }

      it 'redirects to the link\'s original url' do
        expect(response).to redirect_to link.original_url
      end
    end

    context 'invalid request' do
      before { get "/#{SecureRandom.hex(6)}", headers: headers }

      it 'returns 404 status code' do
        expect(response).to have_http_status(404)
      end
    end
  end

end
