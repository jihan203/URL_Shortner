# == Schema Information
#
# Table name: links
#
#  id           :bigint(8)        not null, primary key
#  original_url :text(65535)
#  short_url    :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe Link, type: :model do
  # Validation tests
  it { should validate_presence_of(:original_url) }
  it { should validate_presence_of(:short_url) }
  it { should validate_uniqueness_of(:original_url) }
  it { should validate_uniqueness_of(:short_url) }

  context "new token class method" do
    it "returns a random token starts with the last link's id incremented by 1" do
      create(:link)
      expect(Link.new_token).to start_with (Link.last.id + 1).to_s
    end
  end

end
