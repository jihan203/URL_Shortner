module RequestSpecHelper

  # Parse JSON response to ruby hash
  def json
    JSON.parse(response.body)
  end

  # Returns valid headers
  def valid_headers
    {
      "Content-Type" => "application/json"
    }
  end

end
