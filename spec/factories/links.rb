# == Schema Information
#
# Table name: links
#
#  id           :bigint(8)        not null, primary key
#  original_url :text(65535)
#  short_url    :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryBot.define do
  factory :link do
    sequence(:original_url){ |l| "https://railsconf.com/program/#{l}" }
    short_url "#{Link.new_token}"
  end
end
